<?php

require('animal.php');  
require('ape.php');
require('frog.php');



$sheep = new Animal("Shaun");
echo "Nama          : " . $sheep->nama . "<br>";
echo "Legs          : " . $sheep->legs . "<br>";
echo "Cold_blooded  : " . $sheep->cold_blooded. "<br>";   

echo "<br><br>";

$sungokong = new Ape("Kera Sakti");
echo "Nama          : " . $sungokong->nama . "<br>";
echo "Legs          : " . $sungokong->legs . "<br>";
echo "Cold_blooded  : " . $sungokong->cold_blooded . "<br>";    
echo $sungokong->yell('Auuoooo') . "<br>"; //menggunakan function dari external

echo "<br><br>";

$kodok = new Frog("Buduk");
echo "Nama          : " . $kodok->nama . "<br>";
echo "Legs          : " . $kodok->legs . "<br>";
echo "Cold_blooded  : " . $kodok->cold_blooded. "<br>"; 
echo $kodok->jump() . " <br>";             //menggunakan functuon dari dalam oop itu sendiri

?>
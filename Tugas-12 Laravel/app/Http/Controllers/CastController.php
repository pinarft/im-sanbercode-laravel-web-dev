<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    //create
    public function create()
    {
    return view('cast.tambah');
    }

    //post
    public function store (Request $request)
    {
        $request->validate([
            'name' => 'required',
            'umur' => 'required|numeric',
            'bio'  => 'required',
        ]);
        DB::table('cast')->insert([
            'name'=> $request['name'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
        ]);
       return redirect('/cast'); 
    }

    //tampil data
    public function index()
    {
        $cast = DB::table('cast')->get();

        return view('cast.tampil',['cast'=> $cast]);
    }
    public function show ($id)
    {
        $cast = DB::table('cast')->find($id);
        return view('cast.detail',['cast'=>$cast]);
    }
    public function edit ($id)
    {
        $cast = DB::table('cast')->find($id);
        return view('cast.edit',['cast'=>$cast]);
    }
    public function update($id, Request $request)
    {
        $request->validate([
            'name' => 'required',
            'umur' => 'required|numeric',
            'bio'  => 'required',
        ]);
        DB::table('cast')
        ->where('id',$id)
        ->update(
            [
                'name'=> $request['name'],
                'umur'=> $request['umur'],
                'bio'=>$request['bio']  
            ]
            );
            return redirect('/cast');
    }
    public function delete($id)
    {
        DB::table('cast')
            ->where('id', '=' ,$id)->delete();

        return redirect('/cast');
    }
}

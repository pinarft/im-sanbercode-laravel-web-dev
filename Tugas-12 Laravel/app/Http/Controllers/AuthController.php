<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    //controller get
    function regis()
    {
        return view('page.register');
    }

    function data_table()
    {
        return view('page.data-table');
    }
    function table()
    {
        return view('page.table');
    }




    //controller post
    function kirim(request $request)
    {   
        $firstname = $request['firstname'];
        $lastname = $request['lastname'];
        $gender = $request['gender'];
        $nasional = $request['nasional'];
        $bahasa = $request['bahasa'];

        return view("page.welcome",
        [
            'firstname'=>$firstname,
            'lastname'=>$lastname,
            'gender'=>$gender,
            'nasional'=>$nasional,
            'bahasa'=>$bahasa
        ]);
    }
}

<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;
use  App\Http\Controllers\CastController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//GET
Route::get('/', [HomeController::class, 'index']);
Route::get('/register', [AuthController::class, 'regis']);
Route::get('/data-table', [AuthController::class, 'data_table']);
Route::get('/tabel', [AuthController::class, 'table']);

//POST
Route::post('/welcome', [AuthController::class, 'kirim']);


//CRUD
//Create
Route::get('/cast/create',[CastController::class, 'create']);


//Post
Route::post('/cast',[CastController::class, 'store']);



//ReadData
Route::get('/cast', [CastController::class, 'index']);
Route::get('/cast/{id}', [CastController::class, 'show']);

//update data
Route::get('/cast/{id}/edit',[CastController::class, 'edit']);
Route::put('/cast/{id}',[CastController::class, 'update']);

//delete
Route::delete('/cast/{id}',[CastController::class, 'delete']);
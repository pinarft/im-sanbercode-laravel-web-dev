@extends('layout.master')
@section('title')
    Halaman Register    
@endsection

@section('name')
    ini subtitle halam utama
@endsection

@section('content')


    <h1>Register</h1>
    <p>Tolong diisi biodata dibawah</p>

    <form action="/welcome" method="post">

        @csrf

        <label for="">First Name :</label>
        <br>
        <input type="text" name="firstname" id="" placeholder="Full Name">
        <br>
        <label for="">Last Name :</label>
        <br>
        <input type="text" name="lastname" id="" placeholder="Full Name">
        <br>
        <label for="">Gender :</label>
        <br>
        <input type="radio" name="gender" value="1">Male
        <br>
        <input type="radio" name="gender" value="2">Female
        <br>
        <label for="">Nationality :</label>
        <br>
        <select name="nasional" id="" style="width:100px;">
            <option value="indonesia">Indonesia</option>
            <option value="english">English</option>
            <option value="jepang">Jepang</option>
        </select>
        <br>
        <label for="">Language Spoken :</label>
        <br>
        <input type="checkbox" name="bahasa" id="" value="1">Bahasa Indonesia
        <br>
        <input type="checkbox" name="bahasa" id="" value="2">English <br>
        <input type="checkbox" name="bahasa" id="" value="3">Other
        <br>
        <label for="">Bio :</label>
        <br>
        <textarea name="alamat" id="" cols="30" rows="10" placeholder="Wajib di isi"></textarea>
        <br>
        <input type="submit" value="Kirim">
    </form>
 @endsection
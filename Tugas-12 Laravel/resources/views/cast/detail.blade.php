@extends('layout.master')
@section('title')
    Halaman Cast    
@endsection

@section('subtitle')
    Detail Cast
@endsection


@section('content')
<a href="/cast" class="btn btn-danger">Back</a>
<br><br>
<h1>Nama = {{$cast->name}}</h1>
<h2>Umur = {{$cast->umur}}</h2>
<p>Bio = {{$cast->bio}}</p>
@endsection
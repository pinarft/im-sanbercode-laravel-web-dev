@extends('layout.master')
@section('title')
    Halaman Cast    
@endsection

@section('subtitle')
    Tabel Cast
@endsection


@section('content')

<a href="/cast/create"><button class="btn btn-primary my-3">Tambah Data</button></a>

        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col" class="text-center">Action</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($cast as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->name}}</td>
                        <td class="text-center">
                            <form action="/cast/{{$value->id}}" method="POST">
                                @method('delete')
                                @csrf
                                <a href="/cast/{{$value->id}}" class="btn btn-info btn-sm mr-2">detail</a>
                                <a href="/cast/{{$value->id}}/edit" class="btn btn-warning btn-sm mr-2">Edit</a>
                                <input type="submit" value="Delete" class="btn btn-danger btn-sm ">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>

@endsection
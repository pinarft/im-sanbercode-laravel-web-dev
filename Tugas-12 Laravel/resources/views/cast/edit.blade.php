@extends('layout.master')
@section('title')
    Halaman Cast    
@endsection

@section('subtitle')
    Edit Cast
@endsection


@section('content')
<div>
    <h2>Tambah Data</h2>
        <form action="/cast/{{$cast->id}}" method="POST">
            @csrf
            @method('put')
            <div class="form-group">
                <label>name</label>
                <input type="text" class="form-control" value="{{$cast->name}}" name="name" id="title" placeholder="Masukkan Title">
                @error('name')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>umur</label>
                <input type="text" class="form-control" value="{{$cast->umur}}" name="umur" id="body" placeholder="Masukkan Body">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>bio</label>
                <textarea type="text" class="form-control"  name="bio" id="body" placeholder="Masukkan Body">{{$cast->bio}}</textarea>
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
            <a href="/cast" class="btn btn-danger">Cancle</a>
            
        </form>
</div>  
@endsection